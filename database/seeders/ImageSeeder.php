<?php

namespace Database\Seeders;

use App\Models\Image;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;


// class ImageSeeder extends Seeder
// {
//     public function run()
//     {
//         $products = Product::all();

//         foreach ($products as $product) {
//             $image = new Image([
//                 'url' => '/images/ropa.jpg',
//                 'is_preview' => true,
//             ]);
//             $product->images()->save($image);
//         }
//     }
// }


class ImageSeeder extends Seeder
{
    public function run()
    {
        $products = Product::with('category')->get();  

        foreach ($products as $product) {
            if (!$product->category) {
                continue;  
            }

            $categoryName = strtolower($product->category->name); 
            $imageFiles = File::files(public_path("images/{$categoryName}"));

            foreach ($imageFiles as $file) {
                $image = new Image([
                    'url' => "/images/{$categoryName}/" . $file->getFilename(),
                    'is_preview' => true,  
                ]);
                $product->images()->save($image);
            }
        }
    }
}
