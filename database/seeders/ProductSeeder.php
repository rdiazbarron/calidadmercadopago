<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Image;
use App\Models\State;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $sneakers = Product::factory(3)->create(['category_id' => 1 , 'state_id' => 2]);
        $boots = Product::factory(2)->create(['category_id' => 2 , 'state_id' => 1]);
        $shorts = Product::factory(1)->create(['category_id' => 3 , 'state_id' => 1]);
        $jeans = Product::factory(4)->create(['category_id' => 4 , 'state_id' => 1]);
        $shirts = Product::factory(2)->create(['category_id' => 5 , 'state_id' => 1]);
        $hoodies = Product::factory(2)->create(['category_id' => 6 , 'state_id' => 2]);
        $sweatshirts = Product::factory(4)->create(['category_id' => 7 , 'state_id' => 1]);
        $hats = Product::factory(5)->create(['category_id' => 8 , 'state_id' => 1]);

    }
}
