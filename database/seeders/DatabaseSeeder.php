<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'name' => 'user',
            'last_name' => 'lastname',
            'email' => 'user@mail.com',
            'telephone' => '1234567890',
            'address' => 'user address',
            'city' => 'user city',
            'date_of_birth' => '1990-01-01',
        ]);

        $this->call(CategorySeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(StateSeeder::class);
        $this->call(ImageSeeder::class);
    }
}



