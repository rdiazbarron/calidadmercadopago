import { createStore } from 'vuex';
import VuexPersistence from 'vuex-persist';

const vuexLocal = new VuexPersistence({
    storage: window.localStorage,
    reducer: state => ({ user: state.user }) // persistir solo el estado del usuario
});

export default createStore({
    state: {
        user: null
    },
    mutations: {
        setUser(state, user) {
            state.user = user;
        },
        setRole(state, newRole) {
            if (state.user) {
                //state.user.role_id = newRole;
                state.user = {...state.user, role_id: newRole};
            }
        }
        
    },
    actions: {
        login({ commit }, user) {
            commit('setUser', user);
        },
        logout({ commit }) {
            commit('setUser', null); // Establece el usuario como nulo
        },
        changeUserRole({ commit }, newRole) {
            commit('setRole', newRole);
        }
    },
    getters: {
        role_id: state => state.user ? state.user.role_id : null
    },
    plugins: [vuexLocal.plugin]
});