<?php

namespace App\Http\Controllers;

use App\Enums\OrderStatus;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Payment;
use Illuminate\Http\Request;


class CheckoutController extends Controller
{
    public function store(Request $request)
    {
        $products = Cart::getContent();
        $total = $request->input('total');
        $stripe_session_id = rand(100000, 999999);
        $order = Order::create([
            'user_id' => auth()->id(),
            'status' => OrderStatus::Paid,
            'total' => $total,
            'stripe_session_id' => $stripe_session_id,
        ]);
        
        foreach ($products as $product) {
            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $product->id,
                'quantity' => $product->pivot->quantity,
                'unit_price' => $product->price,
            ]);
        }
        foreach ($products as $product){
            Payment::create([
                'user_id' => auth()->id(),
                'order_id'=> $order->id,
                'total_amount' => $total,
                'status'=> OrderStatus::Paid,
                'stripe_session_id'=>$stripe_session_id,
                'type' => 'card',
            ]);
        }
        return redirect()->route('checkout.success', ['order' => $order->id]);
    }

    public function index(){
        $products = Cart::getContent();
        $savedProducts = Cart::getContent('saved');
        $total = Cart::getCartTotal($products);

        return inertia('Checkout/Checkout', [
            'products' => $products,
            'savedProducts' => $savedProducts,
            'total' => $total,
        ]);
    }
    public function success(Request $request)
    {
        Cart::empty();
        return inertia('Checkout/Success', [
            'order' => Order::find($request->input('order')),
        ]);
    }
}
