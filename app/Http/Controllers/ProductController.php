<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Category;
use App\Models\State;
use Illuminate\Support\Facades\Storage;
//@return \Illuminate\Http\RedirectResponse

use App\Models\Image;

    class ProductController extends Controller
    {
        public function show(Product $product)
        {
            return inertia('Product/Show', [
                'product' => new ProductResource($product->load('category', 'state', 'images'))
            ]);
        }

        public function store(Request $request)
        {
            $request->validate([
                'name' => 'required|string|max:255',
                'description' => 'required|string',
                'price' => 'required|numeric',
                'available_quantity' => 'required|integer',
                'category_id' => 'required|integer',
                'state_id' => 'required|integer',
                'images' => 'required|array',
                'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048' 
            ]);

            $product = new Product();
            $product->name = $request->name;
            $product->description = $request->description;
            $product->price = $request->price;
            $product->available_quantity = $request->available_quantity;
            $product->category_id = $request->category_id;
            $product->state_id = $request->state_id;
            $product->save();

            
    
        if ($request->hasFile('images')) { // 'images' es un array de archivos
            foreach ($request->file('images') as $imageFile) { // Iterar sobre cada archivo
                $path = $imageFile->store('images/products', 'public'); // Guardar cada imagen

                $image = new Image();
                $image->url = Storage::url($path); // Usar Storage::url para obtener la URL pública
                $image->is_preview = true; // Suponiendo que quieres marcar todas las imágenes como vista previa
                $product->images()->save($image);//sE inserta en la bd
            }
        }
        //Esta relación polimórfica "uno a muchos" permite que diferentes modelos (como Product, User, Post, etc.) 
        //tengan imágenes asociadas, utilizando una sola tabla images. La columna imageable_type en la tabla 
        //images almacenará el nombre de la clase
        //del modelo relacionado (por ejemplo, Product), y la columna imageable_id almacenará el ID del modelo relacionado.
        return redirect()->route('dashboard');
            
        }
        public function create()
            {
                $categories = Category::all();
                $states = State::all();

                return inertia('AddProduct', [
                    'categories' => $categories,
                    'states' => $states,
                ]);
            }


            public function destroy(Product $product)
            {
                $product->delete();

                // Puedes agregar un mensaje de éxito o redirigir a una ruta específica
                    return redirect()->route('shop')->with('success', 'Producto eliminado correctamente.');
            }

            public function edit(Product $product)
                {
                    $categories = Category::all();
                    $states = State::all();

                    return inertia('EditProduct', [
                        'product' => $product,
                        'categories' => $categories,
                        'states' => $states,
                    ]);
                }
            public function update(Request $request, Product $product)
            {
                $request->validate([
                    'name' => 'required|string|max:255',
                    'description' => 'required|string',
                    'price' => 'required|numeric',
                    'available_quantity' => 'required|integer',
                    'category_id' => 'required|integer',
                    'state_id' => 'required|integer',
                    'images' => 'sometimes|array',
                    'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                ]);
                $product->update($request->only(['name', 'description', 'price', 'available_quantity', 'category_id', 'state_id']));

                // Manejo de imágenes
                if ($request->hasFile('images')) {
                    // Eliminar imágenes antiguas si es necesario
                    foreach ($product->images as $image) {
                        Storage::delete($image->url); // Asegúrate de que esta ruta sea correcta
                        $image->delete();
                    }
            
                    // Subir nuevas imágenes
                    foreach ($request->file('images') as $imageFile) {
                        $path = $imageFile->store('images/products', 'public'); // Asegúrate de que esta configuración sea correcta
                        $product->images()->create([
                            'url' => Storage::url($path)
                        ]);
                    }
                }
            
          

    }
    }
