<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class DashboardSettingsProfileController extends Controller
{
    public function edit()
    {
        return inertia('Dashboard/Settings/Profile');
    }

    public function update(Request $request)
    {
        $user = auth()->user();
        User::where('id', $user->id)->update($request->all());

        return back()->with('message', 'You have updated your profile.');
    }
}
