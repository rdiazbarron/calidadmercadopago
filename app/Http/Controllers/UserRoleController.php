<?php
namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;

class UserRoleController extends Controller
{
    public function toggleRole(Request $request)
    {
        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(['error' => 'Usuario no encontrado'], 404);
        }

        
        if ($user->role_id == 1) {
            $user->role_id = 2;
        } else {
            $user->role_id = 1;
            
        }
        $user->save();
        

        return response()->json(['success' => 'Rol actualizado', 'new_role' => $user->role_id]);
    }
}