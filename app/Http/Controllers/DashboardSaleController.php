<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\OrderProductResource;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use Carbon\CarbonPeriod;

class DashboardSaleController extends Controller
{

    public function index(Request $request)
    {
        
        $orderP = Order::query()
        ->select(
            'order_product.id', 
            'order_product.quantity', 
            'order_product.unit_price',
            'orders.created_at as fecha', 
            'orders.status as orders_status', 
            'products.name as productsName', 
            'categories.name as categoriesName',
            'payments.type as paymentsType',
            DB::raw('SUM(order_product.unit_price * order_product.quantity) AS total_amount')
        )
        ->join('order_product', 'orders.id', '=', 'order_product.order_id')
        ->join('products', 'products.id', '=', 'order_product.product_id')
        ->join('categories', 'categories.id', '=', 'products.category_id')
        ->join('payments', 'orders.id', '=', 'payments.order_id')
        ->where('orders.status', 'paid')
        ->groupBy(
            'orders.id',
            'orders.created_at', 
            'orders.status', 
            'products.name', 
            'categories.name',
            'order_product.id', 
            'order_product.quantity', 
            'order_product.unit_price',
            'payments.type'
        )
        ->withSortBy($request->sortBy ?? '')
        ->get();
        return inertia('Dashboard/Sale', [
            'orderP' => $orderP,
        ]);
    }
}


   

