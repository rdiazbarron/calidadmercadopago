<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    protected $table = 'payments';
    protected $fillable = [
        'user_id',
        'order_id',
        'total_amount',
        'status',
        'stripe_session_id',
        'type',
    ];
}
