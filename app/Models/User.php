<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'telephone',
        'address',
        'city',
        'date_of_birth',
        'role_id',
    ];

    public function cart()
    {
        return $this->belongsToMany(Product::class, 'carts')
        //Indica que un usuario puede tener muchos productos en su carrito de compras, 
        //y un producto puede estar en el carrito de muchos usuarios
            ->withPivot(['quantity', 'saved_for_later'])
            //Especifica que en la tabla pivote carts habrá columnas adicionales llamadas quantity y saved_for_later,
            // que almacenarán información adicional sobre la relación entre el usuario 
            //y el producto (por ejemplo, la cantidad de ese producto en el carrito y si está guardado para después).
            ->withTimestamps()//tendrá columnas created_at y updated_at para registrar los timestamps de la relación.
            ->using(Cart::class);//Especifica que la relación utilizará el modelo Cart como modelo pivote personalizado.
    }



    public function whishlist()
    {
        return $this->belongsToMany(Product::class, 'whishlists')
            ->withTimestamps()
            ->using(Whishlist::class);
    }
    


    public function orders()
    {
        return $this->hasMany(Order::class);
    }



    public function payments()
    {
        return $this->hasMany(Payment::class);
    }


}
