<?php

namespace Tests\Unit;

use App\Models\Category;
use App\Models\State;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StateTest extends TestCase
{
    use RefreshDatabase;

    public function test_state_has_many_products()
    {
        $state = State::factory()->create();

        $this->assertInstanceOf(Collection::class, $state->products);
    }
}
